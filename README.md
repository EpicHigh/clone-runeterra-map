# Runeterra Map Clone

This is a clone of the map from the game Legends of Runeterra.

## Getting Started

Run the following commands to get started:

```bash
pnpm install
```

Start the development server:

```bash
pnpm dev
```

## Building for Production

To build an optimized version of your app for production, run:

```bash
pnpm build
```

## Visit the deployed site

[https://clone-runeterra-map.vercel.app/](https://clone-runeterra-map.vercel.app/)
