import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import svgr from 'vite-plugin-svgr'
import tsconfigPaths from 'vite-tsconfig-paths'

// https://vitejs.dev/config/

export default defineConfig({
  plugins: [
    react({
      jsxImportSource: '@emotion/react',
      babel: {
        plugins: ['@emotion/babel-plugin'],
      },
    }),
    svgr(),
    tsconfigPaths(),
  ],
  build: {
    sourcemap: true,
    rollupOptions: {
      external: [
        '@safe-{}/safe-core-sdk',
        '@safe-{}/safe-ethers-adapters',
        '@safe-{}/safe-ethers-lib',
      ],
    },
  },
  define:
    process.env.NODE_ENV === 'production'
      ? {}
      : {
          global: {},
          'process.env': {},
        },
})
