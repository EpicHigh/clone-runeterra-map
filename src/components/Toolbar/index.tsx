import styled from '@emotion/styled'
import { ReactComponent as Logo } from '@/assets/logo.svg'

const Container = styled.div`
  flex-direction: column;
  height: 100%;
  width: 50px;
  display: flex;
  justify-content: space-between;
  position: absolute;
  top: 0;
  z-index: 1;

  ::before {
    content: '';
    background-color: var(--rich-black);
    box-sizing: border-box;
    height: 100%;
    position: absolute;
    width: 100%;
    opacity: 0.6;
  }
`

const Link = styled.a`
  fill: var(--light-french-beige);
  align-items: center;
  display: flex;
  height: 50px;
  justify-content: center;
  position: relative;
  transition: fill 0.2s;
  width: 50px;

  &:hover {
    fill: var(--white-chocolate);
  }
`

const LogoIcon = styled(Logo)`
  height: 26px;
  width: 26px;
`

export function Toolbar() {
  return (
    <Container>
      <div>
        <Link
          href="https://universe.leagueoflegends.com/en_US/"
          rel="noreferrer noopener"
          target="_blank"
        >
          <LogoIcon />
        </Link>
      </div>
    </Container>
  )
}
