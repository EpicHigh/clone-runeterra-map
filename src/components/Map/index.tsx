import { Terrain } from './Terrain'
import { Region } from './Region'
import { Controls } from './Control'

interface MapProps {
  onClick: (index: number) => void
  isAuth: boolean
}

export function Map(props: MapProps) {
  const { onClick, isAuth } = props

  return (
    <>
      <Terrain />
      <Region onClick={onClick} />
      <Controls isAuth={isAuth} />
    </>
  )
}
