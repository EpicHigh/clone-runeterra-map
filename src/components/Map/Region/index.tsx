import {
  REGION_SHIELD_POSITIONS,
  REGION_NAMES,
  REGION_SHIELD_PATHS,
  REGION_TERRITORY_PATHS,
} from './constants'
import { useFrame, useLoader, useThree } from '@react-three/fiber'
import { useState } from 'react'
import { TextureLoader } from 'three'
import { Vector3 } from '@react-three/fiber/dist/declarations/src/three-types'
import { RegionTerrain } from './RegionTerrain'

interface RegionProps {
  onClick: (index: number) => void
}

export function Region (props: RegionProps) {
  const { onClick } = props
  const textures = useLoader(TextureLoader, REGION_SHIELD_PATHS)
  const terrainTextures = useLoader(TextureLoader, REGION_TERRITORY_PATHS)
  const { camera } = useThree()

  const [terrainOpacities, setTerrainOpacities] = useState([
    ...Array(9).fill(0),
  ])

  const [hide, setHide] = useState(false)

  const handleHover = (index: number, callback: (index: number) => void) => {
    if (index === 4) return
    if (index > 4) return callback(index - 1)
    callback(index)
  }

  const handleLeave = (index: number, callback: (index: number) => void) => {
    if (index === 4) return
    if (index > 4) return callback(index - 1)
    callback(index)
  }

  const handleHoverTerrain = (index: number) => {
    const opacities = [...terrainOpacities]
    opacities[index] = 0.7
    setTerrainOpacities(opacities)
  }

  const handleLeaveTerrain = (index: number) => {
    const opacities = [...terrainOpacities]
    opacities[index] = 0
    setTerrainOpacities(opacities)
  }


  useFrame(() => {
    setHide(camera.position.z < 1)
  })

  return (
    <group>
      {textures.map((texture, index) => (
        <sprite
          key={index}
          onClick={() => onClick(index)}
          onPointerOver={() => handleHover(index, handleHoverTerrain)}
          onPointerLeave={() => handleLeave(index, handleLeaveTerrain)}
          scale={[0.25, index === 4 ? 0.5 : 0.25, 0]}
          position={[...REGION_SHIELD_POSITIONS[index], 0.3] as Vector3}
        >
          <spriteMaterial map={texture} opacity={hide ? 0 : 1} />
        </sprite>
      ))}
      {REGION_NAMES.map((terrain, index) => (
        <RegionTerrain key={terrain} terrain={terrain} index={index} hide={hide} />
      ))}
      {terrainTextures.map((texture, index) => (
        <sprite key={index} scale={[6.9, 6.6, 0]} position={[0, 0, 0.3]}>
          <spriteMaterial
            map={texture}
            opacity={hide ? 0 : terrainOpacities[index]}
          />
        </sprite>
      ))}
    </group>
  )
}
