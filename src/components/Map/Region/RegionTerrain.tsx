import { REGION_NAME_POSITIONS } from './constants'
import { Vector3 } from '@react-three/fiber/dist/declarations/src/three-types'
import TextTexture from '@seregpie/three.text-texture'

interface RegionTerrainProps {
  hide: boolean
  terrain: string
  index: number
}

export function RegionTerrain(props: RegionTerrainProps) {
  const { hide, terrain, index } = props
  const texture = new TextTexture({
    fontSize: 16,
    text: terrain.toUpperCase(),
    strokeColor: '#392618',
    strokeWidth: 0.06,
    color: '#F0E6D2',
    fontFamily: 'Beaufort',
    fontStyle: 'normal',
  })
  texture.redraw()

  return (
    <sprite
      scale={[terrain.length / 16, 0.2, 0]}
      position={[...REGION_NAME_POSITIONS[index], 0.3] as Vector3}
    >
      <spriteMaterial map={texture} opacity={hide ? 0 : 1} />
    </sprite>
  )
}
