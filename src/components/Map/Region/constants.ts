
export const REGION_SHIELD_POSITIONS = [
  [-1.3, 1],
  [-0.03, 0.6],
  [2.16, 0.85],
  [-1.6, 0.25],
  [0.65, -0.4],
  [1.9, -0.60],
  [-0.85, -1.35],
  [0.2, -1.25],
  [1, -1.2],
  [2.45, -1.35],
]

export const REGION_NAMES = [
  'freljord',
  'noxus',
  'ionia',
  'demacia',
  'piltover & zaun',
  'bilgewater',
  'targon',
  'shurima',
  'ixtal',
  'shadow isles',
] as const

export const REGION_NAME_POSITIONS = [
  [-1.3, 0.85],
  [-0.03, 0.45],
  [2.16, 0.7],
  [-1.6, 0.1],
  [0.65, -0.65],
  [1.9, -0.75],
  [-0.85, -1.5],
  [0.2, -1.4],
  [1, -1.35],
  [2.45, -1.5],
]

export const REGION_SHIELD_PATHS = [
  '/images/regions/freljord.png',
  '/images/regions/noxus.png',
  '/images/regions/ionia.png',
  '/images/regions/demacia.png',
  '/images/regions/piltover-zaun.png',
  '/images/regions/bilgewater.png',
  '/images/regions/targon.png',
  '/images/regions/shurima.png',
  '/images/regions/ixtal.png',
  '/images/regions/shadow-isles.png',
]

export const REGION_TERRITORY_PATHS =  [
  '/images/territory/freljord.png',
  '/images/territory/noxus.png',
  '/images/territory/ionia.png',
  '/images/territory/demacia.png',
  '/images/territory/bilgewater.png',
  '/images/territory/targon.png',
  '/images/territory/shurima.png',
  '/images/territory/ixtal.png',
  '/images/territory/shadow-isles.png',
]

export type RegionName = typeof REGION_NAMES[number]
