import { OrbitControls } from '@react-three/drei'
import { useFrame, useThree } from '@react-three/fiber'
import { MOUSE, Quaternion } from 'three'

interface ControlProps {
  isAuth: boolean
}

export function Controls(props: ControlProps) {
  const { isAuth } = props
  const { camera, gl } = useThree()

  useFrame(() => {
    if (camera.position.z < 1) {
      const quaternion = new Quaternion(0.5, 0, 0, 2)
      camera.applyQuaternion(quaternion)
      camera.quaternion.normalize()
    }
  })

  return (
    <OrbitControls
      minDistance={0.5}
      maxDistance={4.5}
      minAzimuthAngle={0}
      maxAzimuthAngle={0}
      dampingFactor={0.2}
      autoRotate={false}
      enableRotate={false}
      enablePan={isAuth}
      enableZoom={isAuth}
      zoomSpeed={0.5}
      mouseButtons={{
        LEFT: MOUSE.PAN,
      }}
      args={[camera, gl.domElement]}
      enableDamping
    />
  )
}
