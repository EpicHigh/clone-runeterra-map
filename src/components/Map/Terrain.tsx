import { Plane } from '@react-three/drei'
import { useLoader } from '@react-three/fiber'
import { TextureLoader } from 'three'

export function Terrain() {
  const [height, texture, specular] = useLoader(TextureLoader, [
    '/images/map/depth_z1.jpg',
    '/images/map/terrain_z1.jpg',
    '/images/map/ocean_z1.jpg',
  ])

  return (
    <mesh>
      <Plane args={[7.4, 7, 1000, 1000]}>
        <meshPhongMaterial
          attach="material"
          map={texture}
          displacementMap={height}
          displacementScale={1.5}
          specularMap={specular}
        />
      </Plane>
    </mesh>
  )
}
