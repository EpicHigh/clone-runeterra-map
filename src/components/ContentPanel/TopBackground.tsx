import { RegionName } from '@/components/Map/Region/constants'
import { css } from '@emotion/react'
import { ReactComponent as Freljord } from '@/assets/freljord-region-bg.svg'
import { ReactComponent as Noxus } from '@/assets/noxus-region-bg.svg'
import { ReactComponent as Ionia } from '@/assets/ionia-region-bg.svg'
import { ReactComponent as Demacia } from '@/assets/demacia-region-bg.svg'
import { ReactComponent as PiltoverZaun } from '@/assets/piltover-zaun-region-bg.svg'
import { ReactComponent as Bilgewater } from '@/assets/bilgewater-region-bg.svg'
import { ReactComponent as Targon } from '@/assets/targon-region-bg.svg'
import { ReactComponent as Shurima } from '@/assets/shurima-region-bg.svg'
import { ReactComponent as Ixtal } from '@/assets/ixtal-region-bg.svg'
import { ReactComponent as ShadowIsles } from '@/assets/shadow-isles-region-bg.svg'

interface TopBackgroundProps {
  region?: RegionName
}

const styles = css`
  flex-grow: 0;
  flex-shrink: 0;
`

export function TopBackground(props: TopBackgroundProps) {
  const { region } = props

  switch (region) {
    case 'freljord':
      return <Freljord css={styles} />
    case 'noxus':
      return <Noxus css={styles}/>
    case 'ionia':
      return <Ionia css={styles}/>
    case 'demacia':
      return <Demacia css={styles}/>
    case 'piltover & zaun':
      return <PiltoverZaun css={styles}/>
    case 'bilgewater':
      return <Bilgewater css={styles}/>
    case 'targon':
      return <Targon css={styles}/>
    case 'shurima':
      return <Shurima css={styles}/>
    case 'ixtal':
      return <Ixtal css={styles}/>
    case 'shadow isles':
      return <ShadowIsles css={styles}/>
    default:
      return <></>
  }
}
