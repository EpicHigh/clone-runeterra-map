import styled from '@emotion/styled'

const Container = styled.div`
  margin-bottom: 24px;
  align-items: center;
  display: flex;
`

const Image = styled.img`
  display: block;
  flex-shrink: 0;
  height: 60px;
  margin-right: 30px;

  .secondary {
    margin-left: 6px;
  }
`

const TitleSection = styled.div`
  flex-grow: 1;
`

const RegionName = styled.h1`
  font-weight: bold;
  letter-spacing: 0.05em;
  line-height: 100%;
  text-transform: uppercase;
  font-size: 30px;
`

const RegionDescription = styled.p`
  font-weight: bold;
  letter-spacing: 0.05em;
  line-height: 100%;
  text-transform: uppercase;
  font-size: 12px;
  color: var(--grullo);
`

export interface PreviewProps {
  shieldLogo: string
  shieldLogo2?: string
  regionName: string
  regionDescription: string
}

export function Preview(props: PreviewProps) {
  const { shieldLogo, shieldLogo2, regionName, regionDescription } = props

  return (
    <Container>
      <Image src={shieldLogo} />
      {shieldLogo2 && <Image className="secondary" src={shieldLogo2} />}
      <TitleSection>
        <RegionName>{regionName}</RegionName>
        <RegionDescription>{regionDescription}</RegionDescription>
      </TitleSection>
    </Container>
  )
}
