import { motion } from 'framer-motion'
import styled from '@emotion/styled'
import { TopBackground } from './TopBackground'
import { Preview, PreviewProps } from './Preview'
import { Detail, DetailProps } from './Detail'
import { DiamondButton } from '@/components/ContentPanel/DiamondButton'
import { MouseEventHandler } from 'react'
import { RegionName } from '@/components/Map/Region/constants'

const Container = styled(motion.div)`
  height: 100%;
  width: 400px;
  bottom: 0;
  color: var(--white-chocolate);
  display: flex;
  flex-direction: column;
  position: absolute;
  right: 0;
  z-index: 1;
`

const Background = styled(motion.div)`
  display: flex;
  filter: drop-shadow(0 0 3px rgba(0, 0, 0, 0.5));
  flex-direction: column;
  height: 100%;
  position: absolute;
  width: 100%;
`

const BottomBackground = styled.div`
  border-left: 1px solid #1e2328;
  background-color: #010a13;
  flex-grow: 1;
`

const Content = styled.div`
  height: 100%;
  overflow-x: hidden;
  padding: 40px 40px 0;
  position: relative;
  -webkit-overflow-scrolling: touch;
`

export interface ContentPanelProps {
  preview: PreviewProps
  detail: DetailProps
  onClick?: MouseEventHandler<HTMLButtonElement>
  name?: RegionName
}

export function ContentPanel(props: ContentPanelProps) {
  const { preview, detail, onClick, name } = props
  return (
    <Container
      key="content-panel"
      initial={{ x: 400 }}
      animate={{ x: 0 }}
      exit={{ x: 400 }}
      transition={{ duration: 0.5, ease: 'easeInOut' }}
    >
      <Background>
        <TopBackground region={name} />
        <BottomBackground />
      </Background>
      <Content>
        <Preview {...preview} />
        <Detail {...detail} />
      </Content>
      <DiamondButton onClick={onClick} />
    </Container>
  )
}
