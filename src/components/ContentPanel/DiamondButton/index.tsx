import styled from '@emotion/styled'
import { MouseEventHandler } from 'react'

const Container = styled.div`
  left: 0;
  top: 50px;
  position: absolute !important;
  z-index: 1;
  transition: transform 0.2s;
  height: 0;
  width: 0;
`

const Button = styled.button`
  background-color: #010a13;
  border: 1px solid #1e2328;
  cursor: pointer;
  filter: drop-shadow(0 0 3px rgba(0, 0, 0, 0.5));
  height: 26px;
  position: absolute;
  transform: translate(-50%, -50%) rotate(45deg);
  width: 26px;
`

const Chevron = styled.figure`
  pointer-events: none;
  left: 50%;
  margin: 0;
  position: absolute;
  top: 50%;

  ::before {
    transform: translate(1px, -1px) rotate(135deg);
    height: 6px;
    transform-origin: 1px 1px;
    width: 2px;
    background-color: var(--tan);
    content: '';
    position: absolute;
    transition: transform 0.2s, transform-origin 0.2s, background-color 0.2s, width 0.2s, height 0.2s;
  }

  ::after {
    transform: translate(0.25px, 0.25px) rotate(45deg);
    height: 6px;
    transform-origin: 1px 1px;
    width: 2px;
    background-color: var(--tan);
    content: '';
    position: absolute;
    transition: transform 0.2s, transform-origin 0.2s, background-color 0.2s, width 0.2s, height 0.2s;
  }
`

interface DiamondButtonProps {
  onClick?: MouseEventHandler<HTMLButtonElement>
}

export function DiamondButton(props: DiamondButtonProps) {
  const { onClick } = props
  return (
    <Container>
      <Button onClick={onClick} />
      <Chevron />
    </Container>
  )
}
