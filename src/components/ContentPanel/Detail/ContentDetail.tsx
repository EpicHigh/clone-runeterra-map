import styled from '@emotion/styled'

const Container = styled.div`
  margin: 30px 0;
`

const Content = styled.p`
  font-family: 'Spiegel', Helvetica, sans-serif, microsoft yahei;
  font-size: 14px;
  line-height: 150%;
  color: var(--grullo);
`

export interface ContentDetailProps {
  content: string
}

export function ContentDetail(props: ContentDetailProps) {
  const { content } = props
  return (
    <Container>
      <Content>{content}</Content>
    </Container>
  )
}
