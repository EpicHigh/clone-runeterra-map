import { Divider } from './Divider'
import { Stat, StatProps } from './Stat'
import { ContentDetail, ContentDetailProps } from './ContentDetail'

export interface DetailProps extends ContentDetailProps {
  stat?: StatProps
}

export function Detail(props: DetailProps) {
  const { stat, content } = props

  return (
    <div>
      {stat && (
        <>
          <Divider />
          <Stat {...stat} />
        </>
      )}
      <Divider />
      <ContentDetail content={content} />
      <Divider />
    </div>
  )
}
