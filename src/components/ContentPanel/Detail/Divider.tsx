import styled from '@emotion/styled'

const Container = styled.figure`
  transform: translateZ(0);
  position: relative;
  width: 100%;
`

const Middle = styled.div`
  border-top: 1px solid var(--dark-gunmetal);
  margin: 0 10.6066px;
`

const Left = styled.div`
  left: 0;
  transform: translateY(0.5px) rotate(-45deg);
  transform-origin: 0 0;
  border: 1px solid var(--dark-gunmetal);
  box-sizing: border-box;
  height: 8px;
  position: absolute;
  top: 0;
  width: 8px;
`

const Right = styled.div`
  right: 0;
  transform: translateY(0.5px) rotate(45deg);
  transform-origin: 100% 0;
  border: 1px solid var(--dark-gunmetal);
  box-sizing: border-box;
  height: 8px;
  position: absolute;
  top: 0;
  width: 8px;
`

export function Divider() {
  return (
    <Container>
      <Middle />
      <Left />
      <Right />
    </Container>
  )
}
