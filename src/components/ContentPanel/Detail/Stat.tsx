import styled from '@emotion/styled'

const StatList = styled.ul`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin: 30px 0 24px;
`

const StatItem = styled.li`
  box-sizing: border-box;
  flex-grow: 0;
  flex-shrink: 0;
  width: calc(50% - 20px * 0.5);
`

const StatLabel = styled.h3`
  font-family: 'Spiegel', Helvetica, sans-serif, microsoft yahei;
  font-size: 12px;
  line-height: 150%;
  color: var(--grullo);
  margin-bottom: 3px;
`

const StatValue = styled.h4`
  font-weight: bold;
  letter-spacing: 0.05em;
  line-height: 100%;
  text-transform: uppercase;
  font-size: 12px;
  color: #f0e6d2;
  margin-bottom: 6px;
`

export interface StatProps {
  governance: string
  attitudeTowardsMagic: string
  levelOfTechnology: string
  generalEnvironment: string
}

export function Stat(props: StatProps) {
  const { governance, attitudeTowardsMagic, levelOfTechnology, generalEnvironment } = props
  return (
    <StatList>
      <StatItem>
        <StatLabel>Governance:</StatLabel>
        <StatValue>{governance}</StatValue>
      </StatItem>
      <StatItem>
        <StatLabel>Attitude towards magic:</StatLabel>
        <StatValue>{attitudeTowardsMagic}</StatValue>
      </StatItem>
      <StatItem>
        <StatLabel>Level of technology:</StatLabel>
        <StatValue>{levelOfTechnology}</StatValue>
      </StatItem>
      <StatItem>
        <StatLabel>General environment:</StatLabel>
        <StatValue>{generalEnvironment}</StatValue>
      </StatItem>
    </StatList>
  )
}
