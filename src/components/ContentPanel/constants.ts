import { RegionName } from '@/components/Map/Region/constants'
import { ContentPanelProps } from './index'

export const CONTENT_PANEL: Record<RegionName, ContentPanelProps> =
  {
    freljord: {
      preview: {
        shieldLogo: 'https://map.leagueoflegends.com/images/freljord.png',
        regionName: 'Freljord',
        regionDescription: 'Harsh frozen land',
      },
      detail: {
        stat: {
          governance: 'Tribal matriarchy',
          attitudeTowardsMagic: 'Venerate',
          levelOfTechnology: 'Low',
          generalEnvironment: 'Icy tundra',
        },
        content:
          'The Freljord is a harsh and unforgiving land, where demi-gods walk the earth and the people are born warriors. While there are many individual tribes, the three greatest are the Avarosans, the Winter’s Claw, and the Frostguard, each uniquely shaped by their will to survive. It is also the only place on Runeterra where True Ice can be found.',
      },
    },
    noxus: {
      preview: {
        shieldLogo: 'https://map.leagueoflegends.com/images/noxus.png',
        regionName: 'Noxus',
        regionDescription: 'Brutal expansionist empire',
      },
      detail: {
        stat: {
          governance: 'Expansionist empire',
          attitudeTowardsMagic: 'Weaponize',
          levelOfTechnology: 'Medium',
          generalEnvironment: 'Inhospitable steppes',
        },
        content:
          'Noxus is a brutal, expansionist empire, yet those who look beyond its warlike exterior will find an unusually inclusive society. Anyone can rise to a position of power and respect if they display the necessary aptitude, regardless of social standing, background, or wealth. Noxians value strength above all, though that strength can manifest in many different ways.',
      },
    },
    ionia: {
      preview: {
        shieldLogo: 'https://map.leagueoflegends.com/images/ionia.png',
        regionName: 'Ionia',
        regionDescription: 'The first lands',
      },
      detail: {
        stat: {
          governance: 'Regional provinces',
          attitudeTowardsMagic: 'Harmonize',
          levelOfTechnology: 'Low',
          generalEnvironment: 'Magical (varied)',
        },
        content:
          'Known as the First Lands, Ionia is an island continent of natural beauty and magic. Its inhabitants, living in loosely collected provinces, are a spiritual people, seeking harmony with the world. They remained largely neutral until their land was invaded by Noxus—this brutal occupation forced Ionia to reassess its place in the world, and its future path remains undetermined.',
      },
    },
    demacia: {
      preview: {
        shieldLogo: 'https://map.leagueoflegends.com/images/demacia.png',
        regionName: 'Demacia',
        regionDescription: 'Proud military kingdom',
      },
      detail: {
        stat: {
          governance: 'Feudal monarchy',
          attitudeTowardsMagic: 'Deny',
          levelOfTechnology: 'Medium',
          generalEnvironment: 'Fertile countryside',
        },
        content:
          'Demacia is a proud, lawful kingdom with a prestigious military history. Founded as a refuge from magic after the Rune Wars, some might suggest that the golden age of Demacia has passed, unless it proves able to adapt to a changing world. Self-sufficient and agrarian, its society is inherently defensive and insular, valuing justice, honor, and duty above all else.',
      },
    },
    'piltover & zaun': {
      preview: {
        shieldLogo: 'https://map.leagueoflegends.com/images/piltover.png',
        shieldLogo2: 'https://map.leagueoflegends.com/images/zaun.png',
        regionName: 'Piltover & Zaun',
        regionDescription: 'Dual city-states',
      },
      detail: {
        content:
          'Dual city-states that control the major trade routes between Valoran and Shurima. Home both to visionary inventors and their wealthy patrons, the divide between social classes is becoming more dangerous.',
      },
    },
    bilgewater: {
      preview: {
        shieldLogo: 'https://map.leagueoflegends.com/images/bilgewater.png',
        regionName: 'Bilgewater',
        regionDescription: 'Lawless port city',
      },
      detail: {
        stat: {
          governance: 'Gang syndicates',
          attitudeTowardsMagic: 'Employ',
          levelOfTechnology: 'Medium',
          generalEnvironment: 'Tropical archipelago',
        },
        content:
          'Bilgewater is a port city like no other—home to monster hunters, dock-gangs, indigenous peoples, and traders from across the known world. Almost anything can be purchased here, from outlawed hextech to the favor of local crime lords. There is no better place to seek fame and fortune, though death lurks in every alleyway, and the law is almost non-existent.',
      },
    },
    targon: {
      preview: {
        shieldLogo: 'https://map.leagueoflegends.com/images/targon.png',
        regionName: 'Targon',
        regionDescription: 'Sprawling western mountains',
      },
      detail: {
        stat: {
          governance: 'Tribal theocracy',
          attitudeTowardsMagic: 'Aspire',
          levelOfTechnology: 'Low',
          generalEnvironment: 'Harsh mountains',
        },
        content:
          'A mountainous and sparsely inhabited region to the west of Shurima, Targon boasts the tallest peak in Runeterra. Located far from civilization, Mount Targon is all but impossible to reach, save by the most determined pilgrims, chasing some soul-deep yearning to reach its summit. Those hardy few who survive the climb return haunted and empty, or changed beyond all recognition.',
      },
    },
    shurima: {
      preview: {
        shieldLogo: 'https://map.leagueoflegends.com/images/shurima.png',
        regionName: 'Shurima',
        regionDescription: 'Fallen desert empire',
      },
      detail: {
        stat: {
          governance: 'Divine empire',
          attitudeTowardsMagic: 'Covet',
          levelOfTechnology: 'Unknown',
          generalEnvironment: 'Arid desert',
        },
        content:
          'Shurima was once a thriving civilization that spanned the southern continent, left in ruins by the fall of its god-emperor. Over millennia, tales of its former glory became myth and ritual. Now, its nomadic inhabitants eke out a life in the deserts, or turn to mercenary work. Still, some dare to dream of a return to the old ways.',
      },
    },
    ixtal: {
      preview: {
        shieldLogo: 'https://map.leagueoflegends.com/images/ixtal.png',
        regionName: 'Ixtal',
        regionDescription: 'Perilous Eastern Jungles',
      },
      detail: {
        stat: {
          governance: 'Magical autocracy',
          attitudeTowardsMagic: 'Control',
          levelOfTechnology: 'Low',
          generalEnvironment: 'Tropical rainforest',
        },
        content:
          'Secluded deep in the wilderness of eastern Shurima, the sophisticated arcology-city of Ixaocan remains mostly free of outside influence. Having witnessed from afar the ruination of the Blessed Isles, and the softening of Buhru culture, the Ixtali view the other factions of Runeterra as little more than upstarts and pretenders, and use their powerful elemental magic to keep any intruders at bay.',
      },
    },
    'shadow isles': {
      preview: {
        shieldLogo: 'https://map.leagueoflegends.com/images/shadow-isles.png',
        regionName: 'Shadow Isles',
        regionDescription: 'Lands shrouded by the Black Mist',
      },
      detail: {
        stat: {
          governance: 'None',
          attitudeTowardsMagic: 'Suffer',
          levelOfTechnology: 'Low',
          generalEnvironment: 'Cursed archipelago',
        },
        content:
          'The Shadow Isles were once a beautiful realm, long since shattered by a magical cataclysm. Now, Black Mist permanently shrouds the land, tainting and corrupting with its malevolent sorcery. Those who perish within it are condemned to become part of it for all eternity… and worse still, each year the Mist extends its grasp to reap more souls across Runeterra.',
      },
    },
  }
