import styled from '@emotion/styled'
import { BeginExploringButton } from './BeginExploringButton'
import { ExploreContainer } from './ExploreContainer'
import { motion, AnimatePresence } from 'framer-motion'

const Container = styled(motion.div)`
  align-items: center;
  background: radial-gradient(transparent, rgba(1, 10, 19, 0.8));
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: space-around;
  position: absolute;
  top: 0;
  user-select: none;
  width: 100%;
  z-index: 1;

  ::before {
    content: '';
  }
`

interface ExploreProps {
  isAuth: boolean
}

export function Explore(props: ExploreProps) {
  const { isAuth } = props

  return (
    <AnimatePresence>
      {!isAuth && (
        <Container
          key="explore"
          initial={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          transition={{ duration: 0.4, ease: 'easeOut' }}
        >
          <ExploreContainer />
          <BeginExploringButton />
        </Container>
      )}
    </AnimatePresence>
  )
}
