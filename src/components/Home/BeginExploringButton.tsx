import styled from '@emotion/styled'
import { ConnectWallet } from '@thirdweb-dev/react'

const Button = styled(ConnectWallet)`
  font-weight: bold;
  letter-spacing: 0.05em;
  line-height: 100%;
  text-transform: uppercase;
  font-size: 14px;
  position: relative;
  background-image: linear-gradient(var(--eerie-black), var(--eerie-black)),
    linear-gradient(var(--satin-sheen-gold), var(--field-drab));
  padding: 20px 30px;
  background-clip: padding-box, border-box;
  background-origin: padding-box, border-box;
  border: 2px solid transparent;
  color: var(--white-chocolate);
  font-style: normal;
  justify-content: center;
  max-width: 500px;
  user-select: none;
  z-index: 0;

  &:active {
    background-image: linear-gradient(
        var(--dark-gunmetal),
        var(--dark-gunmetal)
      ),
      linear-gradient(var(--cafe-noir), var(--field-drab));
    box-shadow: inset 0 0 0 1px var(--eerie-black);
    color: var(--field-drab);
  }
`

export function BeginExploringButton() {
  return <Button btnTitle="Begin exploring" />
}
