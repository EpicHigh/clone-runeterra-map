import styled from '@emotion/styled'
import { ReactComponent as Underline } from '@/assets/underline.svg'
import { motion } from 'framer-motion'

const Container = styled(motion.div)`
  align-items: center;
  display: flex;
  filter: drop-shadow(0 0 10px rgba(1, 10, 19, 0.7));
  flex-direction: column;
  justify-content: flex-end;
  position: relative;
`

const SubTitle = styled.h2`
  font-weight: bold;
  letter-spacing: 0.05em;
  line-height: 100%;
  text-transform: uppercase;
  font-size: 18px;
  color: var(--light-french-beige);
`

const Title = styled.h1`
  font-weight: bold;
  letter-spacing: 0.05em;
  line-height: 100%;
  text-transform: uppercase;
  font-size: 100px;
  color: var(--white-chocolate);
  margin-bottom: 15px;
`

const UnderLineIcon = styled(Underline)`
  display: block;
  fill: none;
  filter: drop-shadow(0 0 6px rgba(1, 10, 19, 0.9));
`

export function ExploreContainer() {
  return (
    <Container
      initial={{ opacity: 0, y: 100 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 1.2, ease: 'easeIn' }}
    >
      <SubTitle>Explore & Discover</SubTitle>
      <Title>Runeterra</Title>
      <UnderLineIcon />
    </Container>
  )
}
