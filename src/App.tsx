import styled from '@emotion/styled'
import { AnimatePresence } from 'framer-motion'
import { Explore } from '@/components/Home'
import { Toolbar } from '@/components/Toolbar'
import { PerspectiveCamera } from '@react-three/drei'
import { Canvas } from '@react-three/fiber'
import { Map } from '@/components/Map'
import { ContentPanel } from '@/components/ContentPanel'
import { RegionName, REGION_NAMES } from '@/components/Map/Region/constants'
import { useEffect, useState } from 'react'
import { CONTENT_PANEL } from '@/components/ContentPanel/constants'
import { useConnectionStatus } from '@thirdweb-dev/react'

const Container = styled.div`
  bottom: 0;
  overflow: hidden;
  position: fixed;
  top: 0;
  width: 100%;
`

const StyledCanvas = styled(Canvas)`
  width: 100%;
  z-index: 0;
`

function App() {
  const [region, setRegion] = useState<RegionName | ''>('')
  const [isAuth, setIsAuth] = useState(false)
  const connectionStatus = useConnectionStatus()

  const handleRegionClick = (index: number) => {
    setRegion(REGION_NAMES[index])
  }

  const handleClosePanel = () => {
    setRegion('')
  }

  useEffect(() => {
    if (connectionStatus === 'connected') {
      setIsAuth(true)
    }
  }, [connectionStatus])

  return (
    <Container>
      <Explore isAuth={isAuth} />
      <Toolbar />
      <StyledCanvas>
        <PerspectiveCamera makeDefault position={[0, 0, 4.5]} />
        <directionalLight position={[0, 0, 1]} intensity={1} />
        <Map onClick={handleRegionClick} isAuth={isAuth} />
      </StyledCanvas>
      <AnimatePresence>
        {region && (
          <ContentPanel
            name={region}
            onClick={handleClosePanel}
            {...CONTENT_PANEL[region]}
          />
        )}
      </AnimatePresence>
    </Container>
  )
}

export default App
