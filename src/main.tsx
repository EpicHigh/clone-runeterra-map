import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import {
  ThirdwebProvider,
  metamaskWallet
} from '@thirdweb-dev/react'
import { Global, css } from '@emotion/react'
import beaufort from '/font/BeaufortforLOL-Bold.otf'
import spiegel from '/font/Spiegel-Regular.otf'

const globalStyle = css`
  :root {
    font-family: "Beaufort", Arial, sans-serif;
    line-height: 1.5;
    font-weight: 400;
    color-scheme: light dark;
    font-synthesis: none;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -webkit-text-size-adjust: 100%;

    --light-french-beige: #C8AA6E;
    --white-chocolate: #F0E6D2;
    --rich-black: #010A13;
    --field-drab: #785A28;
    --eerie-black: #171B21;
    --satin-sheen-gold: #C89B3C;
    --cafe-noir: #463714;
    --dark-gunmetal: #1E2328;
    --grullo: #A09B8C;
    --tan: #CDBE91;
  }

  body {
    margin: 0;
    display: flex;
    place-items: center;
    min-width: 320px;
    min-height: 100vh;
    background-color: var(--eerie-black);
  }

  @font-face {
    font-family: 'Beaufort';
    src: url(${beaufort}) format('opentype');
  };

  @font-face {
    font-family: 'Spiegel';
    src: url(${spiegel}) format('opentype');
  }
`

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <ThirdwebProvider
      supportedWallets={[metamaskWallet()]}
      activeChain='ethereum'
      autoConnect={false}
    >
      <Global styles={globalStyle} />
      <App />
    </ThirdwebProvider>
  </React.StrictMode>
)
